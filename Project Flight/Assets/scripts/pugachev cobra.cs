using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PCM : MonoBehaviour
{
    
    // Start is called before the first frame update
    public GameObject[] Waypoints;
    int currentWP = 0;
    public float speed = 70.0f;
    public float rotSpeed = 1.0f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Quaternion lookatWP = Quaternion.LookRotation(Waypoints[currentWP].transform.position - this.transform.position);

        //this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookatWP, rotSpeed * Time.deltaTime);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
        if (Vector3.Distance(this.transform.position, Waypoints[currentWP].transform.position) < 15)
        {
            if(currentWP <=1)
            {
                Quaternion lookatWP = Quaternion.LookRotation(Waypoints[currentWP].transform.position - this.transform.position);

                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookatWP, rotSpeed * Time.deltaTime);
            }
            if (currentWP == 2)
            {
                this.transform.Rotate(Vector3.right, -45);

            }
            if(currentWP == 3)
            {
                this.transform.Rotate(-75, 0, 0);
            }
            if(currentWP == 4)
            {
                this.transform.Rotate(-90, 0, 0); 
            }
            if(currentWP == 5)
            {
                this.transform.Rotate(-120, 0, 0);
            }
            if(currentWP == 6)
            {
                this.transform.Rotate(-90, 0, 0);
            }
            if(currentWP == 7)
            {
                this.transform.Rotate(-75, 0, 0);
            }
            if(currentWP == 8)
            {
                this.transform.Rotate(-45, 0, 0);
            }
            if(currentWP == 9)
            {
                this.transform.Rotate(0, 0, 0);
            }
            currentWP++;
        }
        //if (currentWP >= Waypoints.Length)
        //{
            //currentWP = 0;
        //}
        //this.transform.LookAt(Waypoints[currentWP].transform);
        
    }
}