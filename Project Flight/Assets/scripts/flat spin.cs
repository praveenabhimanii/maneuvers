using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour
{
    //Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, transform.position - transform.forward* 10f + transform.up* 2f, 5f * Time.deltaTime);
//Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, transform.rotation, 5f * Time.deltaTime);

    // Start is called before the first frame update
    public GameObject[] Waypoints;
    int currentWP = 0;
    public float speed = 70.0f;
    public float rotSpeed = 1.0f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (currentWP == 12)
        {
            speed = 0;
            return;
        }
        if (Vector3.Distance(this.transform.position, Waypoints[currentWP].transform.position) < 15)
        {
            currentWP++;
        }
        if (currentWP >= Waypoints.Length)
        {
            return;
        }
        //this.transform.LookAt(Waypoints[currentWP].transform);
        Quaternion lookatWP = Quaternion.LookRotation(Waypoints[currentWP].transform.position - this.transform.position);

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookatWP, rotSpeed * Time.deltaTime);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}