using UnityEngine;

public class Cam_Movement : MonoBehaviour
{
    public Transform target; // Reference to the plane
    public float distance;// Distance behind the plane
    public float height; // Height above the plane
    public float smoothSpeed = 0.5f;

    void LateUpdate()
    {
        if (target == null)
        {
            return;
        }

        // Calculate the desired position behind the target
        Vector3 desiredPosition = target.position - target.forward;
        desiredPosition.y = target.position.y;

        transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);

        // Look at the target
        transform.LookAt(target);
    }
}
